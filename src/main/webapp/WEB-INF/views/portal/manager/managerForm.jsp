<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="eng" lang="eng">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<title>Manager</title>
<link rel="stylesheet" type="text/css" href="/css/default.css" />	
<link rel="stylesheet" type="text/css" href="/css/guide.css" />	
<link rel="stylesheet" type="text/css" href="/css/content.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>	
<script type="text/javascript" src="/scripts/jquery-ui/jquery.min.js"></script>
<script type="text/javascript" src="/scripts/common/common-ui.js"></script>
<style type="text/css">
</style>
</head>
<body>
<div id="wrap">
    <!-- header -->
    <div id="header"></div>
    <!-- header -->

    <!-- container -->
    <div id="container">

		<!-- lnb -->
		<div class="lnb"></div>
		<!-- lnb -->

		<!-- contents -->
    	<div class="contents">

			<!-- location -->
            <div class="location"><a href="#">Home</a> &gt; <a href="#">Admin</a> &gt; <a href="#"><span class="txt_w">Manager</span></a></div> 
			<!-- location -->

			<!-- table -->	
          	<div class="title">
                <span class="brd_rtop mgb10">
					<span class="btn_orange"><a href="#">List</a></span>
                </span>
            </div>

			<form action="/portal/new" method="post" >
			<!-- table_detail -->
			<table class="brd_detail">
				<caption>Manager Write</caption>
				<colgroup>
					<col width="25%" />
					<col width="75%" />
				</colgroup>
				
				<tbody>
					<tr>
						<th>ID</th>
						<td><input id="userid" name="userId" type="text" class="" style="width:188px;"/><span class="btn_gray mgl5"><a id="idck" href="javascript:void(0)" onclick="checkId(this)">Check ID</a></span></td>
					</tr>
					<tr>
						<th>Manager Name <img src="/images/mone/star.gif" alt=""/></th>
						<td><input name="userName" type="text" class="" style="width:188px;"/></td>
					</tr>
					<tr>
						<th>Manager Group <img src="/images/mone/star.gif" alt=""/></th>
						<td>
							<select name="groupId" style="width:200px;">
								<option>Select Item</option>
								<option>000</option>
								<option>001</option>
								<option>002</option>
								<option>003</option>
								<option>004</option>
								<option>005</option>
								<option>006</option>
								<option>007</option>
								<option>008</option>
								<option>010</option>
								<option>011</option>
								<option>012</option>
								<option>013</option>
								
							</select>
						</td>
					</tr>
					<tr>
						<th>Password <img src="/images/mone/star.gif" alt=""/></th>
						<td><input name="passwd" type="text" class="" style="width:188px;"/></td>
					</tr>
					<tr>
						<th>Retype Password <img src="/images/mone/star.gif" alt=""/></th>
						<td><input name="input" type="password" class="" style="width:188px;"/></td>
					</tr>
					<tr>
						<th>Department</th>
						<td><input name="deptNm" type="text" class="" style="width:188px;"/></td>
					</tr>
					<tr>
						<th>Employee No.</th>
						<td><input name="empNm" type="text" class="" style="width:188px;"/></td>
					</tr>
					<tr>
						<th>Tel.</th>
						<td><input name="tel" type="text" class="" style="width:188px;"/></td>
					</tr>
					<tr>
						<th>E-mail <img src="/images/mone/star.gif" alt=""/></th>
						<td><input name="email" type="text" class="" style="width:188px;"/></td>
					</tr>
					<tr>
						<th>Status</th>
						<td>
							<select name="stat" style="width:200px;">
								<option>Select Item</option>
								<option>Active</option>
								<option>Inactive</option>
							</select>
						</td>
					</tr>
				</tbody>
				
			</table>

            <div class="brd_btn">
				<span class="left">
					<span class="btn_gray2"><a href="#">Reset</a></span>
				</span>
				<span class="right">
					<span class="btn_orange"><button type="submit">save</button></span>
				</span>
            </div>		
			<!-- table_detail -->

			</form>
		</div>	 
		<!-- contents -->
		
	</div>
    <!-- container -->
   
    <!-- footer -->
    <div id="footer"></div>
    <!-- footer -->

</div>
<script type="text/javascript">
var idck = 0;
function checkId() {
    	
        //userid 를 param.
        var userId =  $("#userid").val(); 
        var url = "/manager/"+userId;
        $.ajax({
            async: true,
            type : 'GET',
            url : "/manager/"+userId,
            dataType : "json",
            contentType: "application/json; charset=UTF-8",
            success : function(data) {
                if (data.cnt > 0) {
                    
                    alert("아이디가 존재합니다. 다른 아이디를 입력해주세요.");
                    
                
                }
            },
            error : function(error) {
                
                alert("error : " + error);
            }
        });
    
}
 
</script>
</body>
</html>