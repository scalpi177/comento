package com.ntels.portal.domain;

import java.util.Date;

import com.ntels.portal.status.ManagerStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ManagerVO {

	private String userId;
	private String userName;
	private String groupId;
	private String passwd;
	private String deptNm;
	private String empNm;
	private String tel;
	private String email;
	private ManagerStatus status;
	private int loginFailCount;
	private String passwdDueDate;
	private int passwdChangePeriod;
	private String lastLoginDate;
	private String lastLoginTime;
	private char accountLock;
	private Date loginFailedDate;
	private String stat;
	@Override
	public String toString() {
		return "ManagerVO [userId=" + userId + ", userName=" + userName + ", groupId=" + groupId + ", passwd=" + passwd
				+ ", deptNm=" + deptNm + ", empNm=" + empNm + ", tel=" + tel + ", email=" + email + ", status=" + status
				+ ", loginFailCount=" + loginFailCount + ", passwdDueDate=" + passwdDueDate + ", passwdChangePeriod="
				+ passwdChangePeriod + ", lastLoginDate=" + lastLoginDate + ", lastLoginTime=" + lastLoginTime
				+ ", accountLock=" + accountLock + ", loginFailedDate=" + loginFailedDate + ", stat=" + stat + "]";
	}
		
	
}
