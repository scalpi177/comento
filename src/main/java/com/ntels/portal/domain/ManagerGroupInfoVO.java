package com.ntels.portal.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ManagerGroupInfoVO {
	private String user_group_name;
	private String user_groupo_id;
	private String user_count;

}
