package com.ntels.portal.dao;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Criteria {
	private int pageNum;
	private int amount;
	public Criteria() {
		this(1,10);
	}
	public Criteria(int p, int a) {
		this.pageNum =p;
		this.amount = a;
	}
}
