package com.ntels.portal.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.ntels.portal.domain.ManagerGroupInfoVO;
import com.ntels.portal.domain.ManagerVO;

@Component
public interface ManagerMapper {
	
	public List<ManagerVO> getManagerList();
	public List<String> getUserGroup();
	public Integer getCountByUserId(String userId);
	public ManagerVO getManagerByUserId(String userId);
	public void insertManager(ManagerVO manager);
	public void updateManager(ManagerVO manager);
	public List<ManagerGroupInfoVO> getGroupCount();
}
