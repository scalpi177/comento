package com.ntels.portal.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ntels.portal.dao.ManagerMapper;
import com.ntels.portal.domain.ManagerGroupInfoVO;
import com.ntels.portal.domain.ManagerVO;

import lombok.RequiredArgsConstructor;

@Service
public class ManagerService {

	@Autowired
	private  ManagerMapper mapper;
	
	public List<ManagerVO> getManager(){
		return mapper.getManagerList();
	}
	
	
	public Map<String,Integer> getCountByUserId(String userId) {
		Map<String,Integer> map = new HashMap<>();
		Integer i =(Integer) mapper.getCountByUserId(userId);
		map.put("cnt",i);
		
		return  map ;
	}
	
	public ManagerVO getManagerByUserId(String userId) {
		
		return mapper.getManagerByUserId(userId);
	}
	public void addManager(ManagerVO vo) 
	{
		mapper.insertManager(vo);
	}
	public void updateManager(ManagerVO vo) 
	{
		mapper.updateManager(vo);
	}
	
	public List<String> getUserGroup(){
		return mapper.getUserGroup();
	}
	public List<ManagerGroupInfoVO> getGroupInfo(){
		return mapper.getGroupCount();
	}
}
