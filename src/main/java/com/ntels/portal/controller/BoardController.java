package com.ntels.portal.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.ntels.portal.domain.ManagerGroupInfoVO;
import com.ntels.portal.domain.ManagerVO;
import com.ntels.portal.service.ManagerService;
import com.ntels.portal.status.ManagerStatus;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Controller
@RequestMapping(value = "/portal")
public class BoardController {
	
	@Autowired
	private ManagerService service;
	
	private String thisUrl = "portal/manager";
	@GetMapping(value = "/managerList")
	public String getManagerList(Model model, HttpServletRequest request) {
		List<ManagerVO> list;
		
		try {
			list =service.getManager();
			model.addAttribute("ManagerList", list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return thisUrl + "/managerList";
	}
	
	@GetMapping(value = "/new")
	public String getManagerForm(Model model) {
		
		return thisUrl + "/managerForm";
	}
	
	@PostMapping(value = "/new")
	public String addManager( ManagerVO vo) {
		System.out.println(vo);
		if(vo.getStat()=="Active") {vo.setStatus(ManagerStatus.Y);}else {vo.setStatus(ManagerStatus.N);}
		service.addManager(vo);
		return "redirect:/portal/managerList";
	}
	
	@GetMapping(value = "/edit/{userId}")
	public String getManagerEditForm(@PathVariable("userId") String userId,Model model) {
		
		model.addAttribute("manager", service.getManagerByUserId(userId));
		return thisUrl + "/managerUpdate";
	}
	
	@PostMapping(value = "/edit/{userId}")
	public String updateManager( ManagerVO vo) {
		System.out.println(vo);
		if(vo.getStat()=="Active") {vo.setStatus(ManagerStatus.Y);}else {vo.setStatus(ManagerStatus.N);}
		service.updateManager(vo);
		return "redirect:/portal/managerList";
	}
	
	
	
	


}
