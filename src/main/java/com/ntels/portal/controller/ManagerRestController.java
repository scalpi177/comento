package com.ntels.portal.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.ntels.portal.domain.ManagerCountResVO;
import com.ntels.portal.domain.ManagerGroupInfoVO;
import com.ntels.portal.service.ManagerService;
import com.ntels.portal.status.ManagerCountResStat;

@RestController
public class ManagerRestController {
	
	@Autowired
	private ManagerService service;
	
	@GetMapping("/userGroup")
	public List<String> getUserGroup(){
		return service.getUserGroup();
	}
	
	
	@GetMapping("/manager/{userId}")
	public Map<String,Integer> isExist(@PathVariable("userId") String userId) 
	{
		
		Map map =service.getCountByUserId(userId);
		System.out.println(map.get("cnt"));
		return map;
	}
	
	@GetMapping(value = "/ManagerGroupCount")
	public ResponseEntity<ManagerCountResVO> getManagerGroupCount() throws JsonProcessingException {
		RestTemplate restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		List<ManagerGroupInfoVO> list = service.getGroupInfo();
		Map<String,List<ManagerGroupInfoVO>> map = new HashMap<String, List<ManagerGroupInfoVO>>();
		map.put("data", list);
		
		String json = new ObjectMapper().writeValueAsString(map);
		
		System.out.println(json);
		HttpEntity<String> entity = new HttpEntity<String>(json,headers);
		
		String url ="http://localhost:8080/api/user-count";
		
		ResponseEntity<ManagerCountResVO> response = restTemplate.postForEntity(url, entity,  ManagerCountResVO.class);
		
		return response;
	}
	
	@PostMapping(value = "/api/user-count")
	public ResponseEntity<ManagerCountResVO> clientServer(@RequestBody String json) {
		System.out.println(json);
		ResponseEntity<ManagerCountResVO> res = new ResponseEntity<ManagerCountResVO>(new ManagerCountResVO("0","Success"),HttpStatus.ACCEPTED);
		return res;
	}
}
